# jdMinecraftLauncher

jdMinecraftLauncher is a OpenSource Minecraft Launcher written in Python. You need Java and a Mojang Account to use this programm.

This program uses [minecraft-launcher-lib](https://pypi.org/project/minecraft-launcher-lib) to install and start Minecraft.

You need [Python](https://www.python.org/) to run jdMinecraftLauncher.

## Installation
### Windows
You can find a prebuild portable version on [Sourceforge](https://sourceforge.net/projects/jdminecraftlauncher).

### Linux
You can install jdMinecraftLauncher from [Flathub](https://flathub.org/apps/details/com.gitlab.JakobDev.jdMinecraftLauncher). Users of Arch basee systems can also install it from the [AUR](https://aur.archlinux.org/packages/jdminecraftlauncher).

### From source
Install all needed dependencies before the first run:  
`pip install -r requirements.txt`  
After that, run jdMinecraftLauncher.py with Python.
